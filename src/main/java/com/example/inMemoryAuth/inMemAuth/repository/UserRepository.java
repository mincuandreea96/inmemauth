package com.example.inMemoryAuth.inMemAuth.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.inMemoryAuth.inMemAuth.model.User;

public interface UserRepository extends JpaRepository<User, Long>{

}
