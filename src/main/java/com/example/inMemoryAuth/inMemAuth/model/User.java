package com.example.inMemoryAuth.inMemAuth.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


@Getter
@Setter
@Entity
@ToString
public class User {
  
	@Id
	@GeneratedValue()
	private Long id;
	private String username;
	private String firstname;
	private String lastname;
	private int age;
	private String password;
	public User(String username, String firstname, String lastname, int age, String password) {
		super();
		this.username = username;
		this.firstname = firstname;
		this.lastname = lastname;
		this.age = age;
		this.password = password;
	}
	
	
	
	
	
}
