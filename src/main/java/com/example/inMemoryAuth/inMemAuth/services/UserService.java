package com.example.inMemoryAuth.inMemAuth.services;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.inMemoryAuth.inMemAuth.model.User;
import com.example.inMemoryAuth.inMemAuth.repository.UserRepository;

@Service
@Transactional
public class UserService {

	
	private UserRepository userRepository;
	
	
	public UserService(UserRepository userRepository){
		this.userRepository=userRepository;
	}
	public void saveUser(User user){
		userRepository.save(user);
	}
	
	
}
