package com.example.inMemoryAuth.inMemAuth.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class UserController {
	
	@Value("${welcome.message:test}")
	private String message = "Hello World";
	
	@Secured("authenticated")
	@RequestMapping("/user/welcome")
	public String welcome(Map<String, Object> model) {
		this.message="user welcome";
		model.put("message", this.message);
		return "user/welcomeUser";
	}
	
	@RequestMapping("/admin/welcome")
	public String welcomeAdmin(Map<String, Object> model) {
		
		this.message="Admin hello";
		model.put("message", this.message);
		return "admin/welcomeAdmin";
	}

}
